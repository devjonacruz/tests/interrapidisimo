<?php

use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Course::class, 10)->create();
        App\Models\Course::whereIn('id', [1, 2])->update(['teacher_id' => '2']);
        App\Models\Course::whereIn('id', [3, 4])->update(['teacher_id' => '3']);
        App\Models\Course::whereIn('id', [5, 6])->update(['teacher_id' => '4']);
        App\Models\Course::whereIn('id', [7, 8])->update(['teacher_id' => '5']);
        App\Models\Course::whereIn('id', [9, 10])->update(['teacher_id' => '6']);
    }
}
