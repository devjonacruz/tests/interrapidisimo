<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\UserRole::create(['user_id' => '1', 'role_id' => '1']);
        App\Models\UserRole::create(['user_id' => '2', 'role_id' => '2']);
        App\Models\UserRole::create(['user_id' => '3', 'role_id' => '2']);
        App\Models\UserRole::create(['user_id' => '4', 'role_id' => '2']);
        App\Models\UserRole::create(['user_id' => '5', 'role_id' => '2']);
        App\Models\UserRole::create(['user_id' => '6', 'role_id' => '2']);
    }
}
