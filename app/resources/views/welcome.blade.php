@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Academia Interrapidisimo</div>

                <div class="card-body">
                    Bienvenid@, te invitamos a realizar tu <a href="/register">registro</a> e <a href="/courses">inscribir</a> tus materias.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection