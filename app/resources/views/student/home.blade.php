@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <courses-component inline-template user="{{ Auth::id() }}">
            <div class="col-md-8">
                <div class="card-header mb-3">
                    Listado de cursos
                </div>

                <div class="card mb-3" v-for="course in courses">
                    <div class="card-header" v-text="course.name"></div>

                    <div class="card-body">
                        <h4 v-text="'Integrantes del curso ' + course.name"></h4>
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="user in course.users">
                                    <td v-text="user.name"></td>
                                    <td v-text="user.email"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </courses-component>

    </div>
</div>
@endsection