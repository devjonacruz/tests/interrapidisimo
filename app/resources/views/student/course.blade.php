@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Listado de materias</div>

                <div class="card-body">
                    <courses-component inline-template add="true">
                        <table class="table table-hover table-striped text-center">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Curso</th>
                                    <th>Creditos</th>
                                    <th>Profesor</th>
                                    <th colspan="2">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="course in courses">
                                    <td v-text="course.id"></td>
                                    <td v-text="course.name"></td>
                                    <td v-text="course.credits"></td>
                                    <td v-text="course.teacher"></td>
                                    <td v-text="course.created"></td>
                                    <td>
                                        <button v-if="course.taken == true" class="btn btn-warning"
                                            @click="removeCourse(course.alias, {{ Auth::id() }})">
                                            Curso agregado / Retirar registro?
                                        </button>
                                        <button v-else-if="course.available == true" class="btn btn-success"
                                            @click="addCourse(course.id, {{ Auth::id() }})">
                                            Agregar Curso
                                        </button>
                                        <button v-else class="btn btn-danger">
                                            Curso no disponible
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </courses-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection