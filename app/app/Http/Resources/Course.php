<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class Course extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $users = $this->users->pluck('id')->toArray();

        $taken = Auth::id() ? (in_array(Auth::id(), $users) ? true : false) : true;

        $teachers = Auth::user()->courses->pluck('teacher_id')->toArray();
        $courses = Auth::user()->courses->pluck('id')->toArray();

        $available = $this->teacher && !in_array($this->teacher->id, $teachers) && count($courses) < 3 ? true : false;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'credits' => $this->credits,
            'teacher' => $this->teacher ? $this->teacher->name : 'SIN ASIGNAR',
            'available' => $available,
            'taken' => $taken,
            'users' => $this->users->map->only(['name', 'email']),
            'created' => $this->created_at->diffForHumans(),
            'updated' => $this->updated_at->diffForHumans(),
            'created_at' => $this->created_at->format('d-m-Y'),
            'updated_at' => $this->updated_at->format('d-m-Y'),
        ];
    }
}
