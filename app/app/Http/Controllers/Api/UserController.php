<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use App\Http\Resources\User as UserResources;
use App\Http\Resources\UserCollection;
use App\Models\Course;
use App\Models\UserCourse;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->user
            ->whereHas('roles', function ($query) {
                $query->where('role_id', '3');
            })
            ->orWhereDoesntHave('roles')
            ->get();

        return response()->json(new UserCollection($students));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json(new UserResources($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function addCourse(Request $request, User $user)
    {
        UserCourse::create([
            'user_id' => $user->id,
            'course_id' => $request->course,
        ]);
        return response()->json(null, 201);
    }

    public function removeCourse(Request $request, User $user, Course $course)
    {
        UserCourse::where('user_id', $user->id)
            ->where('course_id', $course->id)
            ->delete();
        return response()->json(null, 204);
    }
}
