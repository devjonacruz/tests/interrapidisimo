<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Course extends Model
{

    protected $fillable = [
        'name',
        'alias',
        'credits',
        'teacher_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $name = str_replace('#', '', $model->name);
            $model->alias = Str::slug($name, '-');
        });
    }

    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function teacher()
    {
        return $this->belongsTo(\App\User::class, 'teacher_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_courses', 'course_id', 'user_id');
    }
}
