<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'api'], function () {
        Route::apiResource('courses', 'Api\CourseController')->names([
            'index' => 'api.courses.index',
            'store' => 'api.courses.store',
            'update' => 'api.courses.update',
            'show' => 'api.courses.show',
            'destroy' => 'api.courses.destroy',
        ]);

        Route::apiResource('users', 'Api\UserController')->names([
            'index' => 'api.users.index',
            'show' => 'api.users.show',
        ])->only(['index', 'show']);
        Route::post('users/{user}/courses', 'Api\UserController@addCourse');
        Route::delete('users/{user}/courses/{course}', 'Api\UserController@removeCourse');
    });

    Route::get('/home', 'Back\HomeController@index')->name('home');
    Route::resource('courses', 'Back\CourseController')->only(['index', 'edit']);
});


Auth::routes();
