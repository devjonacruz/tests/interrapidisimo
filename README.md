# Requisitos

Modificar el archivo /etc/hosts y agregar el dominio interrapidisimo.local

Web Proxy con letsencrypt

`cd ~`

`git clone https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion.git`

`cd docker-compose-letsencrypt-nginx-proxy-companion`

`cp .env.sample .env`

`./start.sh`

`docker-compose up -d --build`

# Instalar proyecto

`cp app/.env.sample app/.env`

`cd docker`

`cp .env.example .env`

`docker-compose up -d --build`

`docker exec -u $(id -u):$(id -g) interrapidisimo-php composer install`

`cd .. && cd app`

`chmod 777 -R storage/`

`docker exec -u $(id -u):$(id -g) interrapidisimo-php php artisan key:generate`

`docker exec -u $(id -u):$(id -g) interrapidisimo-php php artisan migrate --seed`